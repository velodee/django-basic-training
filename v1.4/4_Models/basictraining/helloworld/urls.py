from django.conf.urls import patterns, include, url
from django.views.generic.simple import direct_to_template

from helloworld.views import HelloWorldView

urlpatterns = patterns('helloworld.views',
    url(r'^$', 'sayhello', name='urlname_sayhello'),
    url(r'^goodbye/', 'saygoodbye', name='urlname_saygoodbye'),
    url(r'^say/(?P<what>.+)', 'say', name='urlname_say'),
    url(r'^say-class/(?P<what>.+)', HelloWorldView.as_view(), name='urlname_say_class'),
)
