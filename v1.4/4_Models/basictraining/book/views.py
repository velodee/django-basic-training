from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.contrib.auth.models import User

from book.models import Book


def book_list(request):
    books = Book.objects.all()
    #a = 1
    #if a == 1:
    #    books = books.filter(id__gt=2).order_by('-detail')
    #else:
    #    books = books.filter(title__contains='Book')
    return render_to_response('book/list.html', {
        'books': books,
    })


def book_detail(request, id):
    book = Book.objects.get(id=id)
    return render_to_response('book/detail.html', {
        'book': book,
    })
