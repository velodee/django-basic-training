from django.conf.urls import patterns, include, url
from book.views import book_list, book_detail

urlpatterns = patterns('',
    url(r'^$', book_list, name='urlname_booklist'),
    url(r'^detail/(?P<id>\d+)/$', book_detail, name='urlname_bookdetail'),
)
