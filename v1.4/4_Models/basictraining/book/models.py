from django.db import models
from django.contrib.auth.models import User


class Book(models.Model):
    author = models.ForeignKey(User)
    title = models.CharField(max_length=50, default="Default title",
                             help_text="Title must be short and clear")
    detail = models.TextField(blank=True)

    class Meta:
        ordering = ['title']

    def __unicode__(self):
        return '%s of %s' % (self.title, self.author)
