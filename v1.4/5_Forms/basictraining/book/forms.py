from django import forms
from django.contrib.auth.models import User
from book.models import Book

CHOICES = (
    (1, "Choice 1"),
    (2, "Choice 2"),
)


class BookForm(forms.Form):
    title = forms.CharField()
    detail = forms.CharField(required=False)
    #extra_field = forms.ChoiceField(choices=CHOICES)

    def clean_title(self):
        title = self.cleaned_data.get('title')
        if title and "Book" in title:
            return title
        raise forms.ValidationError("Title must contain word 'Book'")

    def save(self):
        title = self.cleaned_data.get('title')
        detail = self.cleaned_data.get('detail')
        author = User.objects.get(pk=1)  # admin
        book = Book(author=author, title=title, detail=detail)
        book.save()


class BookModelForm(forms.ModelForm):
    #extra_field = forms.ChoiceField(choices=CHOICES,
    #                    widget=forms.RadioSelect(attrs={'class':'special'}))

    class Meta:
        model = Book
        #exclude = ('author',)

    def clean_title(self):
        title = self.cleaned_data.get('title')
        if title and "Book" in title:
            return title
        raise forms.ValidationError("Title must contain word 'Book'")
