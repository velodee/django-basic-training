from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.contrib.auth.models import User
from django.template import RequestContext

from book.models import Book
from book.forms import BookForm as Form
#from book.forms import BookModelForm as Form


def book_list(request):
    books = Book.objects.all()
    #a = 1
    #if a == 1:
    #    books = books.filter(id__gt=2).order_by('-detail')
    #else:
    #    books = books.filter(title__contains='Book')
    return render_to_response('book/list.html', {
        'books': books,
    })


def book_detail(request, id):
    book = Book.objects.get(id=id)
    return render_to_response('book/detail.html', {
        'book': book,
    })


def book_new(request):
    if request.method == "POST":
        form = Form(request.POST)
        #form = Form(request.POST, request.FILES)  # if upload file
        if form.is_valid():
            form.save()
            return HttpResponse("OK. Thanks!")
    else:  # GET
        form = Form()
    return render_to_response('book/new.html', {
        'form': form,
    }, context_instance=RequestContext(request))


# TODO: add class based view version of book_new
