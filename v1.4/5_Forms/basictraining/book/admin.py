from django.contrib import admin
from book.models import Book


class BookAdmin(admin.ModelAdmin):
    pass
    #search_fields = ['title', 'author__username']
    #list_display = ['pk', 'author', 'title']
    #raw_id_fields = ['author']
    #list_editable = ['title']
    #list_filter = ['author']
    #list_per_page = 10

admin.site.register(Book, BookAdmin)
