from django.conf.urls import patterns, include, url
from book.views import book_list, book_detail, book_new

urlpatterns = patterns('',
    url(r'^$', book_list, name='urlname_booklist'),
    url(r'^detail/(?P<id>\d+)/$', book_detail, name='urlname_bookdetail'),
    url(r'^new/$', book_new, name='urlname_booknew'),
)
