from django.http import HttpResponse
from django.views.generic import TemplateView
from django.shortcuts import render_to_response


def sayhello(request):
    return HttpResponse("Hello World from views!")


def saygoodbye(request):
    return HttpResponse("<b>Goodbye</b> from views!")


def say(request, what):
    return render_to_response('helloworld/say.html', {'what': what})


class HelloWorldView(TemplateView):
    template_name = 'helloworld/say.html'

    def get_context_data(self, **kwargs):
        context = super(HelloWorldView, self).get_context_data(**kwargs)
        context['what'] = kwargs.get('what')
        return context

    #def get(self, request, *args, **kwargs):
    #    return super(HelloWorldView, self).get(request, *args, **kwargs)

    #def post(self, request, *args, **kwargs):
    #    return super(HelloWorldView, self).post(request, *args, **kwargs)
