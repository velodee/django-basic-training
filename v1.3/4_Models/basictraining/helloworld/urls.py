from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template

urlpatterns = patterns('helloworld.views',
    url(r'^$', 'sayhello', name='urlname_sayhello'),
    url(r'^goodbye/', 'saygoodbye',  name='urlname_saygoodbye'),
    url(r'^say/(?P<what>.*)', 'say',  name='urlname_say'),
)

