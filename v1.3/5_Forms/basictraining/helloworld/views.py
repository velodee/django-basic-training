from django.http import HttpResponse
from django.shortcuts import render_to_response

def sayhello(request):
    return HttpResponse("Hello World from views!")

def saygoodbye(request):
    return HttpResponse("<b>Goodbye from views!</b>")

def say(request, what):
    return render_to_response('helloworld/say.html', {'what': what})

