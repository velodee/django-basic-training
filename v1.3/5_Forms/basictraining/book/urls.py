from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template
from book.views import bookindex, bookdetail, booknew

urlpatterns = patterns('',
    url(r'^$', bookindex, name='urlname_bookindex'),
    url(r'^detail/(?P<id>\d+)/$', bookdetail, name='urlname_bookdetail'),
    url(r'^new/$', booknew, name='urlname_booknew'),
)

