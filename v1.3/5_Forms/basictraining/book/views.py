# Create your views here.
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.contrib.auth.models import User

from book.models import Book

def bookindex(request):
    books = Book.objects.all()
    #a = 1
    #if a == 1:
    #    books = books.filter(id__gt=2).order_by('-detail')
    #else:
    #    books = books.filter(title__contains='Book')
    return render_to_response('book/bookindex.html', {
        'books': books,
    })

def bookdetail(request, id):
    book = Book.objects.get(id=id)
    return render_to_response('book/bookdetail.html', {
        'book': book,
    })


from django.template import RequestContext
#from book.forms import BookModelForm as Form
from book.forms import BookForm as Form

def booknew(request):
    if request.method == "POST":
        form = Form(request.POST)
        #form = Form(request.POST, request.FILES)  # if upload file
        if form.is_valid():
            form.save()
            return HttpResponse("OK. Thanks!")
    else: #GET
        form = Form()
    return render_to_response('book/booknew.html', {
        'form':form
    }, context_instance=RequestContext(request))

