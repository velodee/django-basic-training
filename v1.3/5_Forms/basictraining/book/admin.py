from django.contrib import admin
from book.models import Book

# Simple
#admin.site.register(Book)

# Controlled via options
class BookAdmin(admin.ModelAdmin):
    raw_id_fields = ['author']
    #pass


admin.site.register(Book, BookAdmin)

