from django import template

register = template.Library()

@register.filter
def contain(value, arg):
    if arg in value:
        return True
    else:
        return False

