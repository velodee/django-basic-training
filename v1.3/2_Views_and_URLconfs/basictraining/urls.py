from django.conf.urls.defaults import *
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    (r'^helloworld/', include('helloworld.urls')),
    (r'^admin/', include(admin.site.urls)),
)

